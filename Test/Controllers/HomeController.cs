﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Test.Models;

namespace Test.Controllers
{
    public class HomeController : Controller
    {
        EmployeeContext db = new EmployeeContext();

        [HttpGet]
        public ActionResult Index()
        {
            IEnumerable<Employee> employees = db.Employees;
            var em = db.Employees.ToList();
            using (EmployeeContext employeeContext = new EmployeeContext())
            {
                for (int i = 0; i < employees.Count(); i++)
                {
                    employees.ToList()[i].State = employeeContext.States.Find(employees.ToList()[i].StateId);
                    employees.ToList()[i].City = employeeContext.Cities.Find(employees.ToList()[i].CityId);
                }
            }
            ViewBag.Employees = employees;
            return View();
        }

        public ActionResult Search(int? state,int? city)
        {
            IQueryable<Employee> employees = db.Employees.Include(p => p.State);
            if (state != null && state != 0)
            {
                employees = employees.Where(p => p.StateId == state);
            }
            if (city != null && city != 0)
            {
                employees = employees.Where(p => p.CityId == city);
            }

            List<State> states = db.States.ToList();
            List<City> cities = db.Cities.ToList();

            states.Insert(0, new State { Name = "Все", Id = 0 });
            cities.Insert(0, new City { Name = "Все", Id = 0 });

            EmployeesListViewModel plvm = new EmployeesListViewModel
            {
                Employees = employees.ToList(),
                States = new SelectList(states, "Id", "Name", 0),
                Cities = new SelectList(cities, "Id", "Name", 0)
            };
            return View(plvm);
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            Employee b = new Employee { Id = id };
            db.Entry(b).State = EntityState.Deleted;
            db.SaveChanges();
            
            return View();
        }

        [HttpGet]
        public ActionResult AddEmployee()
        {
            int selectedIndex = db.States.ToList()[0].Id;
            ViewBag.StateList = new SelectList(db.States, "Id", "Name", selectedIndex);
            ViewBag.CityList = new SelectList(db.Cities.Where(c => c.StateId == selectedIndex), "Id", "Name");
            return View();
        }

        [HttpPost]
        public ActionResult AddEmployee(Employee employee)
        {
            if (ModelState.IsValid == true)
            {
                db.Employees.Add(employee);
                db.SaveChanges();
                using (EmployeeContext employeeContext = new EmployeeContext())
                {
                    employee.State = employeeContext.States.Find(employee.StateId);
                    employee.City = employeeContext.Cities.Find(employee.CityId);
                }
                return View("Access");
            }
            int selectedIndex = db.States.ToList()[0].Id;
            ViewBag.StateList = new SelectList(db.States, "Id", "Name", selectedIndex);
            ViewBag.CityList = new SelectList(db.Cities.Where(c => c.StateId == selectedIndex), "Id", "Name");
            return View(employee);
        }


        public ActionResult GetCities(int id)
        {
            return PartialView(db.Cities.Where(c => c.StateId == id).ToList());
        }

        public ActionResult GetCitiesForSearch(int id)
        {
            List<City> cities = db.Cities.Where(c => c.StateId == id).ToList();
            cities.Insert(0, new City { Name = "Все", Id = 0 });
            return PartialView(cities);
        }
    }
}