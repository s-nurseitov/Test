﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Test.Models
{
    public class State
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public ICollection<City> Cities { get; set; }

        public State()
        {
            Id = ++count;
        }
        static int count = 0;

        public override string ToString()
        {
            return Name;
        }
    }
}