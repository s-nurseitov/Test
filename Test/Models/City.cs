﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Test.Models
{
    public class City
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public int? StateId { get; set; }
        public State State { get; set; }
        public City()
        {
            Id = ++count;
        }
        static int count = 0;
        public override string ToString()
        {
            return Name;
        }
    }
}