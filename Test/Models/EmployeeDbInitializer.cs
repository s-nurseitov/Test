﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Test.Models
{
    public class EmployeeDbInitializer : DropCreateDatabaseAlways<EmployeeContext>
    {
        protected override void Seed(EmployeeContext db)
        {
            List<State> states = new List<State>
            {
                new State { Name="Казахстан" },
                new State { Name="Россия"},
                new State { Name="Украина" },
                new State { Name="Беларусь" },
                new State { Name="Кыргызстан" }
            };

            states[0].Cities = new List<City>
                    {
                        new City
                        {
                            Name = "Астана",
                            StateId = states[0].Id
                        },
                        new City
                        {
                            Name = "Алмата",
                            StateId = states[0].Id
                        }
                    };
            states[1].Cities = new List<City>
                    {
                        new City
                        {
                            Name = "Москва",
                            StateId = states[1].Id
                        },
                        new City
                        {
                            Name = "Санкт-Петербург",
                            StateId = states[1].Id
                        }
                    };
            states[2].Cities = new List<City>
                    {
                        new City
                        {
                            Name = "Киев",
                            StateId = states[0].Id
                        },
                        new City
                        {
                            Name = "Одесса",
                            StateId = states[0].Id
                        }
                    };
            states[3].Cities = new List<City>
                    {
                        new City
                        {
                            Name = "Минск",
                            StateId = states[0].Id
                        },
                        new City
                        {
                            Name = "Брест",
                            StateId = states[0].Id
                        }
                    };
            states[4].Cities = new List<City>
                    {
                        new City
                        {
                            Name = "Бишкек",
                            StateId = states[0].Id
                        },
                        new City
                        {
                            Name = "Ош",
                            StateId = states[0].Id
                        }
                    };
            db.States.AddRange(states);
            db.Employees.Add(new Employee
            {
                FirstName = "Серик",
                SecondName = "Нурсеитов",
                ThirdName = "Жанатович",
                CityId = states[0].Cities.ToList()[0].Id,
                StateId = states[0].Id,
                Mail = "serik_siti@mail.ru",
                MobileNumber = "87053110090"
            });
        }
    }
}