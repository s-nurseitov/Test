﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Test.Models
{
    public class Employee
    {
        public int Id { get; set; }
        [Display(Name = "Имя")]
        [Required(ErrorMessage = "Пожалуйста, введите свое имя")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Пожалуйста, введите свою фамилию")]
        public string SecondName { get; set; }

        [Required(ErrorMessage = "Пожалуйста, введите свое отчество")]
        public string ThirdName { get; set; }

        [Required(ErrorMessage = "Пожалуйста, введите свой номер мобильного телефона")]
        [RegularExpression(@"^\+?(\d[\d-. ]+)?(\([\d-. ]+\))?[\d-. ]+\d$", ErrorMessage = "Вы ввели некорректный номер")]
        public string MobileNumber { get; set; }

        [Required(ErrorMessage = "Пожалуйста, введите свой email")]
        [RegularExpression(".+\\@.+\\..+", ErrorMessage = "Вы ввели некорректный email")]
        public string Mail { get; set; }

        [Required(ErrorMessage = "Пожалуйста, укажите страну")]
        public int? StateId { get; set; }
        public State State { get; set; }

        [Required(ErrorMessage = "Пожалуйста, укажите город")]
        public int? CityId { get; set; }
        public City City { get; set; }

        public Employee()
        {
            Id = ++count;
        }
        static int count = 0;
    }
}